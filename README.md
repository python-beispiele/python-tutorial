# Python Tutorial

Eine spielerische Einführung in Python.

## Hilfreiche Links zum Nachschlagen

* [Python Tutorial bei w3schools](https://www.w3schools.com/python/default.asp)
* [Kompakte Videos zu den Grundlagen auf Youtube](https://www.youtube.com/watch?v=B7G5B8P8k9s&list=PL98qAXLA6afuh50qD2MdAj3ofYjZR_Phn)

## Beispiele

1. [Zahlenraten](https://gitlab.com/python-beispiele/python-zahlenraten)
1. [Galgenraten](https://gitlab.com/python-beispiele/python-galgenraten)
1. [Türme von Hanoi](https://gitlab.com/python-beispiele/python-hanoi)
1. [Kartenspiel Mau-Mau](https://gitlab.com/python-beispiele/python-kartenspiel)
1. [Handelsspiel](https://gitlab.com/python-beispiele/python-handel)
